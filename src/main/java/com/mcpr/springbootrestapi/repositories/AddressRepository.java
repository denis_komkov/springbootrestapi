package com.mcpr.springbootrestapi.repositories;

import com.mcpr.springbootrestapi.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, String> {
}
