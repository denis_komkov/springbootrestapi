package com.mcpr.springbootrestapi.repositories;

import com.mcpr.springbootrestapi.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
}
