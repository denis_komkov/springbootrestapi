package com.mcpr.springbootrestapi.controllers;

import com.mcpr.springbootrestapi.exceptions.AddressNotFoundException;
import com.mcpr.springbootrestapi.entities.Address;
import com.mcpr.springbootrestapi.repositories.AddressRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/rest")
public class AddressController {

    private final AddressRepository repository;

    AddressController(AddressRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/address")
    List<Address> getAddresses() {
        return repository.findAll();
    }

    @GetMapping("/address/{id}")
    Address getAddress(@PathVariable String id) {
        return repository.findById(id)
                .orElseThrow(() -> new AddressNotFoundException(id));
    }

    @PutMapping("/address/{id}")
    Address updateAddress(@RequestBody Address newAddress, @PathVariable String id) {
        return repository.findById(id)
                .map(address -> {
                    address.setStreet(newAddress.getStreet());
                    address.setCity(newAddress.getCity());
                    address.setState(newAddress.getState());
                    address.setPostalCode(newAddress.getPostalCode());
                    address.setCountry(newAddress.getCountry());
                    return repository.save(address);
                })
                .orElseThrow(() -> new AddressNotFoundException(id));
    }

    @DeleteMapping("/address/{id}")
    void deleteAddress(@PathVariable String id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new AddressNotFoundException(id);
        }
    }
}