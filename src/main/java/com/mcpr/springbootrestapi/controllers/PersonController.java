package com.mcpr.springbootrestapi.controllers;

import com.mcpr.springbootrestapi.exceptions.PersonNotFoundException;
import com.mcpr.springbootrestapi.entities.Person;
import com.mcpr.springbootrestapi.repositories.PersonRepository;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path="/rest")
public class PersonController {

    private final PersonRepository repository;

    PersonController(PersonRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/person")
    List<Person> getPersons() {
        return repository.findAll();
    }

    @GetMapping("/person/{id}")
    Person getPerson(@PathVariable String id) {
        return repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
    }

    @PutMapping("/person/{id}")
    Person updatePerson(@RequestBody Person newPerson, @PathVariable String id) {
        return repository.findById(id)
                .map(person -> {
                    person.setName(newPerson.getName());
                    person.setPhone(newPerson.getPhone());
                    person.setEmail(newPerson.getEmail());
                    person.setAddressId(newPerson.getAddressId());
                    return repository.save(person);
                })
                .orElseThrow(() -> new PersonNotFoundException(id));
    }

    @DeleteMapping("/person/{id}")
    void deletePerson(@PathVariable String id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new PersonNotFoundException(id);
        }
    }
}
