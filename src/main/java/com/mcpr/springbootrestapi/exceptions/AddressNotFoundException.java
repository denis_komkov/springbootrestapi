package com.mcpr.springbootrestapi.exceptions;

public class AddressNotFoundException extends RuntimeException{

    public AddressNotFoundException(String id) {
        super("Could not find address by id: " + id);
    }
}
