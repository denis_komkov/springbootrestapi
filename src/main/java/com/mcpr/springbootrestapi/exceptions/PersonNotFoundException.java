package com.mcpr.springbootrestapi.exceptions;

public class PersonNotFoundException extends RuntimeException {

    public PersonNotFoundException(String id) {
        super("Could not find person by id: " + id);
    }
}
