DELETE FROM Address;

INSERT INTO Address (street, city, state, postal_code, country)
VALUES 
('Street1', 'City1', 'State1', 'PostalCode1', 'Country1'),
('Street2', 'City2', 'State2', 'PostalCode2', 'Country2'),
('Street3', 'City3', 'State3', 'PostalCode3', 'Country3');

DELETE FROM Person;

INSERT INTO Person (name, phone, email, address_id)
VALUES
('Name1', 'Phone1', 'Email1', (SELECT Id FROM Address WHERE Street = 'Street1')),
('Name2', 'Phone2', 'Email2', (SELECT Id FROM Address WHERE Street = 'Street2')),
('Name3', 'Phone3', 'Email3', (SELECT Id FROM Address WHERE Street = 'Street3'));