CREATE TABLE IF NOT EXISTS Address
(
	id varchar(36) DEFAULT gen_random_uuid(),
	street varchar(1024) NOT NULL,
	city varchar(1024) NOT NULL,
	state varchar(1024) NOT NULL,
	postal_code varchar(1024) NOT NULL,
	country varchar(1024) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Person
(
	id varchar(36) DEFAULT gen_random_uuid(),
	name varchar(1024) NOT NULL,
	phone varchar(1024) NOT NULL,
	email varchar(1024) NOT NULL,
	address_id varchar(36),
	PRIMARY KEY (id),
	FOREIGN KEY (address_id) REFERENCES Address (id) ON DELETE SET NULL
);